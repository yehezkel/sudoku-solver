const assert = require('assert');
const sudoku = require('../solver.js');

describe('Sudoku',function(){
    describe('#Sudoku',function(){
        it("Constructor should exist",function(){
            assert.doesNotThrow(
                () => {

                    var game = new sudoku.Sudoku(
                        new sudoku.SquareSize()
                    );
                },
                TypeError
            );
        });

    });

    describe('#Board',function(){
        it("Invalid dimensions",function(){
            assert.throws(
                () => {

                    var game = new sudoku.Sudoku(
                        new sudoku.SquareSize()
                    );

                    game.Solve([1,2,3])
                },
                /Board length and dimensions missmatch/

            );
        });

    })

    describe('#Solve',function(){

        const tdt = [
            {
                x:3,y:3,
                board:[
                    0,0,0,      0,5,1,      9,7,0,
                    3,0,7,      9,0,0,      0,0,2,
                    0,0,0,      3,2,0,      1,0,4,

                    0,0,0,      1,9,8,      7,0,0,
                    0,3,0,      0,7,0,      0,2,0,
                    0,0,9,      2,3,6,      0,0,0,

                    9,0,5,      0,4,2,      0,0,0,
                    4,0,0,      0,0,9,      2,0,8,
                    0,8,2,      5,6,0,      0,0,0,
                ], 

                expect:[
                    2,4,8,      6,5,1,      9,7,3,
                    3,1,7,      9,8,4,      5,6,2,
                    5,9,6,      3,2,7,      1,8,4,

                    6,2,4,      1,9,8,      7,3,5,
                    8,3,1,      4,7,5,      6,2,9,
                    7,5,9,      2,3,6,      8,4,1,

                    9,7,5,      8,4,2,      3,1,6,
                    4,6,3,      7,1,9,      2,5,8,
                    1,8,2,      5,6,3,      4,9,7,
                ]
            },
            {
                x:3,y:3,
                board:[
                    0,3,5,      0,0,2,      0,4,0,
                    0,9,0,      1,0,0,      0,3,7,
                    0,0,0,      0,3,0,      0,0,0,

                    3,5,9,      0,7,0,      4,0,0,
                    0,0,0,      3,0,0,      0,1,5,
                    0,0,0,      8,0,5,      0,6,0,

                    0,0,4,      2,0,0,      0,0,0,
                    2,0,7,      4,0,0,      1,0,8,
                    0,8,0,      0,1,6,      9,0,4,
                ], 

                expect:[
                    7,3,5,      9,6,2,      8,4,1,
                    6,9,2,      1,8,4,      5,3,7,
                    1,4,8,      5,3,7,      2,9,6,

                    3,5,9,      6,7,1,      4,8,2,
                    8,2,6,      3,4,9,      7,1,5,
                    4,7,1,      8,2,5,      3,6,9,

                    9,1,4,      2,5,8,      6,7,3,
                    2,6,7,      4,9,3,      1,5,8,
                    5,8,3,      7,1,6,      9,2,4,
                ]
            },

            {
                x:3,y:2,
                board:[
                    0,4,0,      0,0,3,
                    0,0,0,      5,0,2,

                    0,1,0,      2,0,0,
                    0,0,2,      0,3,0,

                    4,0,6,      0,0,0,
                    1,0,0,      0,5,0, 
                ], 

                expect:[
                    2,4,5,      6,1,3,
                    6,3,1,      5,4,2,

                    3,1,4,      2,6,5,
                    5,6,2,      1,3,4,

                    4,5,6,      3,2,1,
                    1,2,3,      4,5,6,
             
                ]
            },
            {
                x:1,y:1,
                board:[0],
                expect:[1]
            }
        ];

        tdt.forEach(function(test){

            it(`Board ${test.x},${test.y}:${test.board}`,function(){
                const square = new sudoku.SquareSize(test.x,test.y)
                solver       = new sudoku.Sudoku(square)

                const solved = solver.Solve(
                    test.board
                );
                
                assert.deepEqual(
                    test.board,
                    test.expect
                );
            })
        })
    })
});
