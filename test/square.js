
const assert = require('assert');
const sudoku = require('../solver.js');

const DEFAULT_SQUARE_X = 3;
const DEFAULT_SQUARE_Y = 3;


describe('SquareSize',function(){
    describe('#SquareSize',function(){
        it("Constructor should exist",function(){
            assert.doesNotThrow(
                () => {
                    var square = new sudoku.SquareSize()
                },
                TypeError
            );
        });

        it("Default constructor parameter",function(){
            
            const square = new sudoku.SquareSize()
            assert.equal(square.x,DEFAULT_SQUARE_X);
            assert.equal(square.y,DEFAULT_SQUARE_Y);
            
        });

        it("rows parameter",function(){
            
            const rows   = 10;
            const square = new sudoku.SquareSize(rows)
            assert.equal(square.x,rows);
            assert.equal(square.y,DEFAULT_SQUARE_Y);
            
        });

        it("column parameter",function(){
            
            const columns   = 10;
            const square    = new sudoku.SquareSize(DEFAULT_SQUARE_X,columns)
            assert.equal(square.x,DEFAULT_SQUARE_X);
            assert.equal(square.y,columns);
            
        });


    });

    describe('#ItemsOnBoard',function(){
        const tdt = [
            {x:3,y:3,expect:81},
            {x:4,y:4,expect:16*16}
        ];

        tdt.forEach(function(test){
            it(`temsOnBoard x:${test.x} y:${test.y} expecting:${test.expect}`,function(){
                const square = new sudoku.SquareSize(test.x,test.y)
                assert.equal(
                    square.ItemsOnBoard(),
                    test.expect
                );
            })
        })
    });
})