function SquareSize(x = 3, y = 3){
    this.x = x;
    this.y = y;
}

SquareSize.prototype.ItemsOnBoard = function (){
    return this.x*this.x*this.y*this.y;
}


function Sudoku(square){

    this.square = square;
}

Sudoku.prototype.Solve = function(board){

    let square = this.square;
    let length = board.length;

    if(square.x < 1 || square.y < 1 || length != square.ItemsOnBoard()){
        throw new Error("Board length and dimensions missmatch")
    }

    let cells = buildBoard(square)
    copyBoard(cells,board)
    
    const solved = solveBoard(cells,square, 0)
    if(solved){
        for (let i = 0; i < length; i++) {
            if(cells[i].value > 0){
                board[i] = cells[i].value
            }
        };
    }

    return solved;

}

function solveBoard(cells, square, pos){

    const length = cells.length;
    if(pos == length){
        return true;
    }

    if(cells[pos].value < 0){
        return solveBoard(cells,square,pos+1)
    }

    const count = square.x*square.y;
    for (let i = 0; i < count; i++) {
        if(cells[pos].SetValue(i+1)){
            if(solveBoard(cells,square,pos+1)){
                return true;
            }
        }
        cells[pos].Clean();
    }

    return false;
}


function buildBoard (square){
    const square_items = square.x * square.y
    const length       = square.ItemsOnBoard()

    let sets = Array(square_items*3)
    for (let i = sets.length - 1; i >= 0; i--) {
        sets[i] =  new UniqueSet(
            square_items + 1
        )
    };

    let groups = [
        new GroupSet(sets.slice(0,square_items),(position) => {
            return Math.floor(position/square_items);
        
        }),
        new GroupSet(sets.slice(square_items,2*square_items),(position) => {
            return position % square_items;
        
        }),
        new GroupSet(sets.slice(2*square_items),(position) => {
            return Math.floor(
                Math.floor(position/square_items)/square.y
            )*square.y + Math.floor((position % square_items)/square.x)
        }),
    ]

    let cells = Array(length)
    for (let i = 0; i < length; i++) {
        let cell = new Cell();
        groups[0].assign(cell,i);
        groups[1].assign(cell,i);
        groups[2].assign(cell,i);
        cells[i] = cell
    };

    return cells;
}

function copyBoard(cells, board){

    const length = board.length;
    for (let i = 0; i < length; i++) {

        if(board[i]){

            if(!cells[i].SetValue(board[i])){
                throw new Error("Invalid Board")
            }

            cells[i].Fix()
        }
    };
}


function Cell(){
    this.value;
    this.sets = [];
}

Cell.prototype.SetValue = function(value){
    let allow = true;
    for(let s of this.sets){
        if(s.HasValue(value)){
            allow = false
            break
        }
    }

    if(allow){
        for(let s of this.sets){
            s.SetValue(value)
        }

        this.value = value;
    }

    return allow;
}

Cell.prototype.Fix =  function(){
    this.value *= -1
}

Cell.prototype.Clean = function(){

    for(let s of this.sets){
        s.Clean(this.value)
    }

    this.value = 0;
}

Cell.prototype.Attach =  function(set){
    this.sets.push(set);
}

function UniqueSet(length){
    this.items = Array(length).fill(false);
}

UniqueSet.prototype.HasValue = function(value){
    return (
        value > 0 && value < this.items.length && this.items[value]
    );
}

UniqueSet.prototype.SetValue = function(value){
    const has = this.HasValue(value)
    if(!has){
        this.items[value] = true;
    }

    return !has;
}

UniqueSet.prototype.Clean = function(value){

    if(value > 0 && value < this.items.length){
        this.items[value] = false;
    }
}

function GroupSet(sets, selectorCb){
    this.sets = sets;
    this.cb   = selectorCb
}

GroupSet.prototype.assign = function(cell, position){
    const index = this.cb(position);

    cell.Attach(
        this.sets[index]
    );
}

module.exports = {
    SquareSize,
    Sudoku
}


